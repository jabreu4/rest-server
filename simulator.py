import logging
from collections import Counter
import random

logger = logging.getLogger(__name__)

class Simulator():
    
    def __init__(self, cnt=0):
        logger.info('Starting simulator module')
        self._blink = False
        self._counter = cnt

    @property
    def counter(self):
        self._counter += 1
        return self._counter

    @property
    def random(self):
        return round(random.random() * 100, 2)

    @property
    def blink(self):
        if not self._blink:
            self._blink = True
        else:
            self._blink = False
        return self._blink

if __name__ == '__main__':
    logging .basicConfig()
    s = Simulator()
    print(f'{s.blink()=}')
    print(f'{s.random()=}')
    print(f'{s.counter()=}')
