from flask import Flask, render_template, jsonify

import settings as sets
import simulator
import realtime

app = Flask(__name__)
sim = simulator.Simulator()


@app.route('/simulator')
def get_simulator_data():
    return jsonify({'counter': sim.counter, 'blink': sim.blink, 'random': sim.random})


@app.route('/realtime')
def hits():
    try:
        return jsonify({'hits': realtime.get_hit_count()})
    except Exception:
        return "#ERR Redis server not found!"


@app.route('/')
def default():
    return render_template('index.html', rest_version=sets.APP_VERSION)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
