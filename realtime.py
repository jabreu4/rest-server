import redis
import settings as sets
import time


r = redis.Redis(host=sets.REDIS_HOST, port=6379, decode_responses=True)

def get_hit_count():
    retries = 5
    while True:
        try:
            return r.incr('hits')
        except redis.exceptions.ConnectionError as ex:
            if retries == 0:
                raise ex
            retries -= 1
            time.sleep(0.5)
            print(f'{sets.REDIS_HOST=} {retries=}')

print('Opening redis connection')